SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";
CREATE TABLE admin (
 id int(11) NOT NULL,
 username varchar(255) NOT NULL,
 password varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE anggota (
 id int(11) NOT NULL,
 nama varchar(255) NOT NULL,
 nik int(11) NOT NULL,
 alamat text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE buku (
 id int(11) NOT NULL,
 judul varchar(255) NOT NULL,
 tahun year(4) NOT NULL,
 penulis varchar(255) NOT NULL,
 status int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE peminjaman (
 peminjaman_id int(11) NOT NULL,
 peminjaman_buku int(11) NOT NULL,
 peminjaman_anggota int(11) NOT NULL,
 peminjaman_tanggal_mulai date NOT NULL,
 peminjaman_tanggal_sampai date NOT NULL,
 peminjaman_status int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
CREATE TABLE petugas (
 id int(11) NOT NULL,
 nama varchar(255) NOT NULL,
 username varchar(255) NOT NULL,
 password varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
ALTER TABLE admin
 ADD PRIMARY KEY (`id`);
ALTER TABLE anggota
 ADD PRIMARY KEY (`id`);
ALTER TABLE buku
 ADD PRIMARY KEY (`id`);
ALTER TABLE peminjaman
 ADD PRIMARY KEY (`peminjaman_id`);

ALTER TABLE petugas
 ADD PRIMARY KEY (`id`);

ALTER TABLE admin
 MODIFY id int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE anggota
 MODIFY id int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE buku
 MODIFY id int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE peminjaman
 MODIFY peminjaman_id int(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE petugas
 MODIFY id int(11) NOT NULL AUTO_INCREMENT;
COMMIT;