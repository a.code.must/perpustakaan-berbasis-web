<?php
class buku_model extends CI_Model {
  public function get_buku() {
    $this->load->database();
    $query = $this->db->get('buku');
    return $query->result();
  }
  public function insert_post($kodeBuku, $namaBuku, $penerbit, $penulis, $tahunTerbit, $isbn) {
    $this->load->database();
    $data = array(
      'kodeBuku' => $kodeBuku,
      'namaBuku' => $namaBuku,
      'penerbit' => $penerbit,
      'penulis' => $penulis,
      'tahunTerbit' => $tahunTerbit,
      'isbn' => $isbn
    );
    $this->db->insert('buku', $data);
  }
  public function delete_buku($kodeBuku) {
    $this->load->database();
    $this->db->delete('buku', array('kodeBuku' => $kodeBuku));
  }
  public function get_this($kodeBuku){
    $this->load->database();
    $query = $this->db->get_where('buku', array('kodeBuku'=>$kodeBuku));
    return $query->result();
  }
  public function update_buku($kodeBuku, $namaBuku, $penerbit, $penulis, $tahunTerbit, $isbn) {
    $this->load->database();
    $data = array(
      'kodeBuku' => $kodeBuku,
      'namaBuku' => $namaBuku,
      'penerbit' => $penerbit,
      'penulis' => $penulis,
      'tahunTerbit' => $tahunTerbit,
      'isbn' => $isbn
    );
    $this->db->where('kodeBuku', $kodeBuku);
    $this->db->update('buku', $data);
  }
}