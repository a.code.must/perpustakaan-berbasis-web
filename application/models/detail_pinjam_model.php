<?php
class detail_pinjam_model extends CI_Model {
  public function get_detail_pinjam() {
    $this->load->database();
    $query = $this->db->get('detail_pinjam');
    return $query->result();
  }
  public function insert_detail_pinjam($noPinjam, $kodeBuku, $status) {
    $this->load->database();
    $data = array(
      'noPinjam' => $noPinjam,
      'kodeBuku' => $kodeBuku,
      'status' => $status
    );
    $this->db->insert('detail_pinjam', $data);
  }
  public function delete_detail_pinjam($noPinjam) {
    $this->load->database();
    $this->db->delete('detail_pinjam', array('noPinjam' => $noPinjam));
  }
  public function get_this($noPinjam){
    $this->load->database();
    $query = $this->db->get_where('detail_pinjam', array('noPinjam'=>$noPinjam));
    return $query->result();
  }
  public function update_detail_pinjam($noPinjam, $kodeBuku, $status) {
    $this->load->database();
    $data = array(
      'noPinjam' => $noPinjam,
      'kodeBuku' => $kodeBuku,
      'status' => $status
    );
    $this->db->where('noPinjam', $noPinjam);
    $this->db->update('detail_pinjam', $data);
  }
}