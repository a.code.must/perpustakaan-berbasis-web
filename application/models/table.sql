CREATE TABLE PETUGAS{
KodePetugas VARCHAR(7) PRIMARY KEY NOT NULL,
NamaPetugas VARCHAR(30) NOT NULL,
AlamatPetugas VARCHAR(30) NOT NULL,
Jabatan VARCHAR(30) NOT NULL
};

CREATE TABLE ANGGOTA{
KodeAnggota VARCHAR(7) PRIMARY KEY NOT NULL,
NamaAnggota VARCHAR(30) NOT NULL,
AlamatAnggota VARCHAR(50) NOT NULL,
NoTelepon VARCHAR(12),
Email VARCHAR(30) NOT NULL
};

CREATE TABLE BUKU{
KodeBuku VARCHAR(7) PRIMARY KEY NOT NULL,
NamaBuku VARCHAR(30) NOT NULL,
Penerbit VARCHAR(50) NOT NULL,
Penulis VARCHAR(30) NOT NULL,
TahunTerbit SMALLINT,
ISBN VARCHAR (50) NOT NULL
};

CREATE TABLE Detail_Pinjam{
NoPinjam Varchar (7) NOT NULL,
KodeBuku VARCHAR (7) NOT NULL,
STATUS INTEGER,
FOREIGN KEY(KodeBuku)
REFERENCES BUKU(KodeBuku),
PRIMARY KEY (NoPinjam, KodeBuku)
};

CREATE TABLE PEMINJAMAN{
NoPinjam Varchar (7) NOT NULL PRIMARY KEY,
KodeAnggota VARCHAR (7) NOT NULL,
KodePetugas VARCHAR (7) NOT NULL,
TglPinjam DATE,
TglKembali DATE,
JumlahPinjam INTEGER
FOREIGN KEY(KodeAnggota)
REFERENCES ANGGOTA(KodeAnggota),
FOREIGN KEY(KodePetugas)
REFERENCES PETUGAS (KodePetugas)
};
