<?php
class petugas_model extends CI_Model {
  public function get_petugas() {
    $this->load->database();
    $query = $this->db->get('petugas');
    return $query->result();
  }
  public function insert_petugas($kodePetugas, $namaPetugas, $alamatPetugas, $jabatan) {
    $this->load->database();
    $data = array(
      'kodePetugas' => $kodePetugas,
      'namaPetugas' => $namaPetugas,
      'alamatPetugas' => $alamatPetugas,
      'jabatan' => $jabatan
    );
    $this->db->insert('petugas', $data);
  }
  public function delete_petugas($kodePetugas) {
    $this->load->database();
    $this->db->delete('petugas', array('kodePetugas' => $kodePetugas));
  }
  public function get_this($kodePetugas){
    $this->load->database();
    $query = $this->db->get_where('petugas', array('kodePetugas'=>$kodePetugas));
    return $query->result();
  }
  public function update_petugas($kodePetugas, $namaPetugas, $alamatPetugas, $jabatan) {
    $this->load->database();
    $data = array(
      'kodePetugas' => $kodePetugas,
      'namaPetugas' => $namaPetugas,
      'alamatPetugas' => $alamatPetugas,
      'jabatan' => $jabatan
    );
    $this->db->where('kodePetugas', $kodePetugas);
    $this->db->update('petugas', $data);
  }
}