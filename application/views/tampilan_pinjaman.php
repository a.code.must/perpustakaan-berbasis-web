<!DOCTYPE html>
<html lang="id">

<head>
  <meta charset="UTF-8">
  <title>Perpustakaan</title>
  <style>
  /* ====GLOBAL STYLE==== */
body {
  background-color: #F8F8F8;
}
div.container {
  width: 960px;
  padding: 10px 50px 20px;
  background-color: white;
  margin: 20px auto;
  box-shadow: 1px 0px 10px, -1px 0px 10px ;
}
h1,h2,h3 {
  text-align: center;
  font-family: Cambria, "Times New Roman", serif;
  clear: both;
}
#footer {
  text-align: right;
  margin-top: 20px;
}

/* =====HEADER===== */
#header {
  height: 60px;
}
#logo {
  font-size: 42px;
  float: left;
  text-shadow: 1px 2px #C0C0C0;
  margin-top: 10px; 
  margin-bottom: 0px;
  }
#logo span {
  color: green;
}
#tanggal{ 
  text-align: right;
}
hr {
  margin: 0px; 
}
  /* =====NAVIGATION===== */
nav {
  width: 500px;
  float: left;
  clear: both;
}
ul{
  padding: 0;
  margin: 20px 0;
  list-style: none;
  overflow: hidden;
}
nav li a {
  float: left;
  background-color: #E3E3E3;
  color: black;
  text-decoration: none;
  font-size: 20px;
  height: 30px;
  line-height: 30px;
  padding: 5px 20px;
}
nav li a:hover {
  background-color: #757575;
  color: white;
}
/* ====SEARCH BOX==== */
#search {
  font-size: 20px;
  width: 340px;
  float: right;
  margin: 20px 0px;
  background-color: #E3E3E3;
}
#search p {
  margin: 0px;
  padding: 8px 20px; 
}
  </style>
</head>

<body>

  <div class="container">
    <div id="header">
      <h1 id="logo">Perpustakaan </h1>
      <p id="tanggal"><?php echo date("d M Y"); ?></p>
    </div>
    <hr>
    <hr>
    <nav>
      <ul>
        <li><a href="tampil_peminjam.php">Tampil</a></li>
        <li><a href="tambah_peminjam.php">Tambah</a>
        <li><a href="edit_anggota.php">Edit</a>
        <li><a href="history.php">History</a></li>
        <li><a href="login.php">Logout</a>
      </ul>
    </nav>
    <form id="search" action="tampil_buku.php" method="get">
      <p>
        <label for="nim">Nama Buku: </label>
        <input type="text" name="nama" id="nama" placeholder="search...">
        <input type="submit" name="submit" value="Search">
      </p>
    </form>
    <h2>Tambah Peminjam Buku</h2>
    <form id="form_peminjaman" action="peminjaman_buku.php" method="post">
      <fieldset>
        <legend>Peminjaman</legend>
        <p>
          <label for="kode">Kode : </label>
          <input type="text" name="kode" id="kode" value="" placeholder="Contoh: 12345678">
          (8 digit angka)
        </p>
        <p>
          <label for="buku">Buku : </label>
          <input type="text" name="buku" id="buku">
        </p>
        <p>
          <label for="Nama">Nama : </label>
          <input type="text" name="nama" id="nama">
        </p>
        <p>
          <div class="form-group">
            <label class="font-weight-bold" for="tanggal_mulai">Tanggal Mulai Pinjam</label>
            <input type="date" class="form-control" name="tanggal_mulai" placeholder="Masukkan tanggal mulai pinjam">
          </div>
        </p>
        <p>
          <div class="form-group">
            <label class="font-weight-bold" for="tanggal_sampai">Tanggal Pinjam Sampai</label>
            <input type="date" class="form-control" name="tanggal_sampai" placeholder="Masukkan tanggal pinjam sampai">
          </div>
        </p>
      </fieldset>
      <br>
      <p>
        <input type="submit" name="submit" class="btn btn-primary" value="Simpan">
      </p>
    </form>


  </div>

</body>

</html>